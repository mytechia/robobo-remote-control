# This repository moved #
Please use the new location: https://github.com/mintforpeople/robobo-remote-control

# ROBOBO Remote Control Library #

Contains the source code of the different modules that made up the ROBOBO remote control library.

Those modules are:

* The remote control module
* The remote ROB module